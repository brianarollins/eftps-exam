# README #


### What is this repository for? ###

This is a testing repo for potential First Data EFTPS front end developers.


### How do I get set up? ###

Just clone the repo and then work on the code you were assigned for the interview.


### How do I turn it in ###

Create your own branch in Git (after cloning) and name it after yourself (first initial, last name) so we know it's yours. Then commit it and push it to the BitBucket repo. Create a Pull Request so we know to review the code.

### How do I know if I have the right answer? ###

There are no right answers when coding. What we're looking for is well-written code. Code that is elegant, easy-to-understand, and easy-to-maintain is what will get you in the door.