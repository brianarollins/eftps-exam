//Remember, you have jQuery 2.2.4 already running.

var data = [
   {
    "firstName":"Rita",
    "lastName":"Jones",
    "dob":"1984-02-16",
    "favoriteColor":"Purple"
},{
    "firstName":"Muhammad",
    "lastName":"Fahri",
    "dob":"1988-11-06",
    "favoriteColor":"Red"
}, {
    "firstName":"John",
    "lastName":"Smith",
    "dob":"1980-04-01",
    "favoriteColor":"Green"
}, {
    "firstName":"Dopinder",
    "lastName":"Kapoor",
    "dob":"1979-06-04",
    "favoriteColor":"Green"
}
];

var showData = function(target){
    for(var i in data) {        
        $(target).append(i.firstName+"<br/>");
    }
};